package com.infostretch.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Streams {
	public static void main(String[] args) {
		List<Integer> list = new ArrayList<Integer>();
		List<String> names = new ArrayList<String>();
		names.add("Abhijit");
		names.add("Ankit");
		names.add("Keyur");
		names.add("KArtik");
		for (int i = 1; i < 6; i++)
			list.add(i);
		System.out.println(list);

		/* Converting Integer list into String List */
		List<String> listString = list.stream().map(i -> i.toString()).collect(Collectors.toList());
		System.out.println(listString);

		/* Printing even numbers using filter */
		System.out.println(list.stream().map(i -> i).filter(number -> number % 2 == 0).collect(Collectors.toList()));
		list.stream().forEach(l -> {
			System.out.print(l);
		});

		/* Printing numbers less than 4 using filter */
		System.out.println();
		System.out
				.println(list.stream().map(number -> number).filter(number -> number < 4).collect(Collectors.toList()));

		/* Printing names starting and ending with specific letter */
		System.out.println(
				names.stream().filter(name -> name.startsWith("A") && name.endsWith("t")).collect(Collectors.toList()));

		/* Converting letters in uppercase */
		System.out.println(names.stream().map(name -> name.toUpperCase()).collect(Collectors.toList()));

		List<Integer> pipes = new ArrayList<>();
		for (int i = 0; i < 30; i++) {
			pipes.add(Integer.valueOf(i));
		}
		System.out.println(pipes);
		List<Integer> answer = pipes.stream()
                .filter(li -> li % 3 == 0)
                .filter(li -> li % 6 == 0)
                .map(li -> li * li)
                .filter(li -> li > 50)
                .map(li -> (int) Math.sqrt(li))
                .collect(Collectors.toList());
        System.out.println(answer);
	}
}
